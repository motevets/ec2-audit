#!/bin/usr/env node
"use strict";

const stringify = require('csv-stringify');
const AWS = require('aws-sdk');
const _ = require('lodash');

AWS.config.update({ "region": 'us-east-1' });

const ec2 = new AWS.EC2();

function getName(instance){
  const nameTag = _(instance.Tags).find( (o) => { return o.Key == 'Name' } ) || {};
  return nameTag.Value;
}

ec2.describeInstances({}, (err, data) => {
  let instanceData = [];
  let csv = "";
  if (err) console.log(err, err.stack); // an error occurred
  _(data.Reservations).each( (reservation) => {
    _(reservation.Instances).each( (instance) => {
      const details = [
        getName(instance),
        instance.InstanceType,
        instance.PublicIpAddress,
        instance.KeyName
      ]
      instanceData.push(details);
    });
  });

  stringify(instanceData, (err, csv) => {
    if (err) {
      console.error(err);
    } else {
      console.log(csv);
    }
  });

});
